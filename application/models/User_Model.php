<?php

class User_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function get_all_users()
	{
		$query = $this->db->get('Users');
		return $query->result_array();
	}

	public function insert_user($data)
	{
		$this->db->insert('Users',$data);
	} // insert user (unused ?)

	public function delete_user($id)
	{
		$this->db->delete('Users', array('user_id' => $id));
	} // delete user (unused ?)

	public function delete_users($data)
	{
		$this->db->trans_start();
		foreach ($data as $val)
		{
			$this->db->where('user_id', $val['user_id']);
			$this->db->delete('Users');
		}
		$this->db->trans_complete();
	}

	public function update_users($data)
	{
		// ToDo !!! IMPORTANT FIXING HERE !!!

		// Data is assosiative array
		$this->db->trans_start();

		foreach ($data as $value)
		{
			if (empty($value['user_id']))
			{
				$this->db->insert('Users', $value);
			}
			else
			{
				$this->db->replace('Users', $value);
			}
		}

		$this->db->trans_complete();
	}

	public function create_table() // debug ? please remove - jere
	{
		// $this->db->query('CREATE TABLE `Users` ( `user_id` INT NOT NULL AUTO_INCREMENT , `user_name` VARCHAR(20) NOT NULL , `user_address` VARCHAR(100) NOT NULL , PRIMARY KEY (`user_id`)) ENGINE = InnoDB;');
	}

}
