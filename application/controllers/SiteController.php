<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SiteController extends CI_Controller
{
	/* Mock data for table */
	protected $data = array();

	function __construct()
	{
		parent::__construct();

		$this->load->model('User_Model'); // Load Model

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index()
	{
		// $this->load->database(); // Unused (Manually Connected)

		// Mock Data for our users
		/*$this->data["Users"] = array(
			array("user_name" => "John", "user_address" => "River Street"),
			array("user_name" => "Bob", "user_address" => "River Street 2"),
			array("user_name" => "Jeff", "user_address" => "River Street 4"),
			array("user_name" => "Mark", "user_address" => "River Street 8"),
			array("user_name" => "Jackson", "user_address" => "River Street 168")
		);*/

		$this->data["Users"] = $this->User_Model->get_all_users();

		$this->load->view('landing_site', $this->data);
	}

	public function fetch()
	{
		if ($this->input->method(TRUE) == 'GET') {
			$this->output
				->set_status_header(401)
				->set_output('nothing here');
			exit;
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($this->User_Model->get_all_users()));
	}

	public function create()
	{
		$this->form_validation->set_rules('user_name', 'Name', 'required');
		$this->form_validation->set_rules('user_address', 'Address', 'required');
		$this->form_validation->set_rules('user_age', 'Age', 'required');

		if ($this->form_validation->run() === FALSE) {
			redirect(base_url('index'));
		} else {
			$user_data = array(
				'user_name' => $this->input->post('user_name'),
				'user_address' => $this->input->post('user_address')
			);
			$this->User_Model->insert_user($user_data);

			redirect(base_url('index'));
		}
	} // unused ?

	public function edit()
	{
		// filter our requests
		if (!$this->input->is_ajax_request()) {
			$this->output->set_status_header('404');
			show_404();
		}

		// Data get returned as post request
		$json_data = json_decode($this->input->post('data', true), true);
		$json_deleted_data = json_decode($this->input->post('deleted_data', true), true);

		if (!empty($json_deleted_data)) {
			// delete user here
			$this->User_Model->delete_users($json_deleted_data);
		}

		// update to database
		$this->User_Model->update_users($json_data);
	}

	public function delete()
	{
		$id = $this->input->post('user_del_id');
		$this->User_Model->delete_user($id);
		redirect(base_url('index'));
	} // unused ?
}
