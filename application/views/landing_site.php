<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<!--Using CDN Bootstrap css and js-->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous"/>

	<!--JQuery-->
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"
			integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

	<!--Using CDN Bootstrap css and js-->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
			crossorigin="anonymous"></script>

	<!--Using Jquery Datatable Library-->
	<link rel="stylesheet" type="text/css"
		  href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.2/b-2.0.0/b-colvis-2.0.0/b-html5-2.0.0/b-print-2.0.0/date-1.1.1/r-2.2.9/sc-2.0.5/sl-1.3.3/datatables.min.css"/>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript"
			src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.2/b-2.0.0/b-colvis-2.0.0/b-html5-2.0.0/b-print-2.0.0/date-1.1.1/r-2.2.9/sc-2.0.5/sl-1.3.3/datatables.min.js"></script>

	<!--Using Jquery Datatable Utility Modified Library-->
	<script type="text/javascript" src="<?php echo base_url('js/dataTables.cellEdit.js'); ?>"></script>

	<title>Landing Page</title>

	<style type="text/css">
		* {
			box-sizing: inherit;
			color: whitesmoke;
		}

		html {
			background-color: transparent;
			box-sizing: border-box;
			font-size: 14px;
		}

		body {
			background-color: #384048;
			color: whitesmoke;
			font: 100%/1.5 Tahoma, Verdana, Segoe, sans-serif;
		}

		.container {
			margin: 0 3rem 3rem;
		}

		.modal-content {
			background-color: #384048;
			color: whitesmoke;
		}

		table.dataTable {
			border: 1px solid #000;
			background-color: #141414;
		}

		.table-striped > tbody > tr:nth-child(odd) {
			background-color: #3d3d3d;
		}

		.table-striped > tbody > tr:nth-child(even) {
			background-color: #525252;
		}

		.table-hover tbody tr:hover {
			background-color: #969696;
		}

		.dataTables_wrapper .dataTables_paginate .paginate_button {
			color: #cacaca !important;
		}

		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
			color: #828282 !important;
		}

		.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate
		.paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
			color: #cecece !important;
		}

		.dataTables_wrapper .dataTables_paginate .paginate_button {

		}

		.text-black {
			color: #0c0c0c;
		}

	</style>
</head>
<body>

<div class="container">
	<h1 class=""> DEMO TABLE </h1>

	<table class="table table-hover table-striped table-bordered table-responsive dataTable" id="table-cstm">
		<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Name</th>
			<th scope="col">Address</th>
			<th scope="col">Age</th>
			<th scope="col">Action</th>
		</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>

<div class="container">

	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal">
		Add New Data
	</button>

	<button id="save_btn" type="button" class="btn btn-success">
		Save
	</button>

	<!-- Modal Add -->
	<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="addModalLabel">Add New Data</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">

					<?php echo form_open('#', 'id=form_add'); ?>

					<div class="mb-3">
						<?php echo form_label('Name:', 'user_name'); ?>
						<?php
						$data_user = array(
								'name' => 'user_name',
								'id' => 'user_name',
								'value' => '',
								'class' => 'form-control'
						);
						$customClass = array(
								'placeholder' => 'Enter Name',
						);
						echo form_input($data_user, '', $customClass); ?>
					</div>
					<div class="mb-3">
						<div>
							<?php echo form_label('Address:', 'user_address'); ?>
							<?php
							$data_user = array(
									'name' => 'user_address',
									'id' => 'user_address',
									'value' => '',
									'class' => 'form-control'
							);
							$customClass['placeholder'] = 'Enter Address';
							echo form_input($data_user, '', $customClass); ?>
						</div>
					</div>
					<div class="mb-3">
						<div>
							<?php echo form_label('Age:', 'user_age'); ?>
							<?php
							$data_user = array(
									'name' => 'user_age',
									'id' => 'user_age',
									'value' => '',
									'class' => 'form-control',
									'type' => 'number'
							);
							$customClass['placeholder'] = 'Enter Age';
							echo form_input($data_user, '', $customClass); ?>
						</div>
					</div>

					<button type="submit" class="btn btn-primary">Submit</button>

					<?php echo form_close(); ?>

				</div>
			</div>
		</div>
	</div>

	<!-- Modal Delete -->
	<div class="modal fade" id="delModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
		 aria-labelledby="delModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="delModalLabel">Remove Data</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<p>Are you sure you wish to remove this data?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
					<?php
					echo form_open('#', 'id=form_delete');
					echo form_hidden('del_id', '');
					?>
					<button type="submit" class="btn btn-danger">Remove</button>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
<script type="text/javascript">
	// On Ready Event
	$(document).ready(function () {

		let deleted_data = [];

		// Load Our data from ajax request
		let table_data = $('#table-cstm').DataTable({
			"autoWidth": true,
			"paging": true,
			"ordering": false,
			"info": false,
			"processing": true,
			"serverSide": false,
			"ajax": {
				url: "/fetch",
				type: "POST",
				dataSrc: ""
			},
			"columns": [
				{ /*data: "user_id"*/
					"className": 'text-center',
					"data": null,
					"render": function (data, type, row, meta) {
						return `<label>${(meta.row) + 1}</label>`;
					}
				},
				{data: "user_name"},
				{data: "user_address"},
				{data: "user_age"},
				{
					"className": 'text-center',
					"data": null,
					"render": function (data, type, full, meta) {
						return `<button type="button" data-bs-toggle="modal" data-bs-target="#delModal"
								class="btn btn-mini btn-danger pull-right">
								Delete</button>`;
					}
				}
			],
			dom: 'Bfrtip',
			buttons: [
				'copy', 'excel', 'pdf'
			]
		});

		table_data.MakeCellsEditable({
			"onUpdate": callbackFunctionTable,
			"inputCss": 'text-black',
			"columns": [1, 2, 3],
			"confirmationButton": false /*{
				"confirmCss " : false,
				"cancelCss": false,
				"listenToKeys": false
			}*/,
			"inputTypes": [
				{
					"column": 1,
					"type": "text",
					"options": null
				},
				{
					"column": 2,
					"type": "text",
					"options": null
				},
				{
					"column": 3,
					"type": "number",
					"options": null
				},
			]
		})

		function callbackFunctionTable(updatedCell, updatedRow, oldValue) {
			// console.log("The new value for the cell is: " + updatedCell.data());
			// console.log("The old value for that cell was: " + oldValue);
			// console.log("The values for each cell in that row are: " + updatedRow.data());
		}

		// Button delete pressed
		$('#table-cstm').on('click', 'button', function (event) {
			// Grab Outer 'tr'
			let rowIndex = table_data.row($(this).closest('tr')).index();

			// push deleted data
			deleted_data.push(table_data.row($(this).closest('tr')).data());

			// input index into hidden input inside form
			$("input[name='del_id']").val(rowIndex)
		})

		// Add Button Pressed
		$('#form_add').on('submit', function (event) {
			event.preventDefault();

			// Grab Form input data
			let formData = new FormData(event.target);

			// Append data into dataTable
			table_data.row.add({
				user_name: formData.get('user_name'),
				user_address: formData.get('user_address'),
				user_age: formData.get('user_age')
			}).draw();

			// Reset Modal Input
			$('#addModal')
				.modal('toggle')
				.find('input,textarea,select')
				.val('')
				.end()
				.find("input[type=checkbox], input[type=radio]")
				.prop("checked", "")
				.end();
		})

		// Delete Button Pressed
		$('#form_delete').on('submit', function (event) {
			event.preventDefault();

			// Grab Form input data
			let formData = new FormData(event.target);

			// Remove row from datatable but don't refresh the page drawn
			table_data.row(formData.get('del_id')).remove().draw(false);

			// toggle modal
			$('#delModal').modal('toggle')
		})

		// Save Button Pressed
		$('#save_btn').click(function () {
			// Get Arrays of data from datatable
			// console.log(table_data.rows().data());
			// console.log('deleted data', deleted_data);

			//ToDo: Ajax Save Here
			$.ajax({
				url: '<?= base_url('update')?>',
				type: 'POST',
				datatype: 'json',
				data: { data: JSON.stringify(table_data.rows().data().toArray()), deleted_data: JSON.stringify(deleted_data) },
				success: function (result) {
					console.log(result);
					table_data.ajax.reload();
				}
			})

			// delete deleted_data
			deleted_data = [];
		});
	});
</script>
</html>
